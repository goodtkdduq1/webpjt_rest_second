FROM openjdk:11
ARG JAR_FILE=build/libs/BackEnd-Second-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} BackEnd-Second-0.0.1-SNAPSHOT.jar.jar
ENTRYPOINT ["java","-jar","/BackEnd-Second-0.0.1-SNAPSHOT.jar.jar"]
EXPOSE 8081
