# LiveRandomChat - RestAPI1


## Link
[1. 사용 언어](#using)

[2. LocalBuild](#localBuild)

[3. CI/CD](#cicd)


## 사용 프레임워크 <a id=using></a>
- 언어 : JAVA
- Spring Boot : 2.3.2
- System 구성 : Front-end Rep 참조 ( https://gitlab.com/goodtkdduq1/webpjt_front-end )
- Cloud Server : AWS EC2 우분투 64bit
- Docker
- 첫번째와 RestAPI와 동일한 방법으로 사용 기술 및 참조는 첫번째 RestAPI Rep 참조 (https://gitlab.com/goodtkdduq1/webpjt)


## Build <a id=localBuild></a>
- Gradle
- application.properties 참조


## CI/CD <a id=cicd></a>
- JenkinsFile 참조
- GitLab PR Merge 시 Jenkins Server(Local PC)에서 Trigger를 활성화하여 Jenkin File 실행
- Jenkins File 구성
    1. gradlew clean / build
    2. 기존 Docker Image 삭제
    3. DockerFile의 구성으로 Docker Image 생성
    4. 만들어진 Docker Image로 Container 실행



