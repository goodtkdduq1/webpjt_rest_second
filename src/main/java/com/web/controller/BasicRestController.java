package com.web.controller;

import com.web.service.QuestionService;
import com.web.dto.QuestionListDto;
import com.web.dto.QuestionDto;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequiredArgsConstructor
public class BasicRestController {
    private final QuestionService questionService;

    @PostMapping("/v1/Question")
    public QuestionListDto addQuestion(@RequestBody @NotNull QuestionDto questionDto){
        questionService.addQuestion(questionDto);
        return questionService.getQuestionAllList();
    }

    @DeleteMapping("/v1/Question")
    public QuestionListDto removeQuestion(@RequestBody QuestionDto questionDto)
    {
        questionService.removeQuestionByRoomId(questionDto.getRoomId());
        return questionService.getQuestionAllList();
    }

    @GetMapping("/v1/questionList")
    public QuestionListDto questionList(){
        return questionService.getQuestionAllList();
    }

    @GetMapping("/v1/questionListByTags")
    public QuestionListDto questionListByTags(@RequestParam(value="tagList[]") ArrayList<String> tags){
        return questionService.getQuestionListByTags(tags);
    }

    @GetMapping("/v1/questionByRoomId/{roomId}")
    public QuestionDto questionByRoomId(@PathVariable int roomId){
        return questionService.getQuestionByRoomId(roomId);
    }
}
