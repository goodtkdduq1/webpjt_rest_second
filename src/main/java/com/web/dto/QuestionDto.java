package com.web.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QuestionDto {
    private String title;
    private String context;
    private int roomId;
    private String mkUser;
    private List<String> tags;

    public int getTagSize(){
        return tags.size();
    }
}
