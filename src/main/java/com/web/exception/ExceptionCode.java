package com.web.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExceptionCode implements CommonExceptionCode {
    QUESTION_ROOM_NOT_FOUND_EXCEPTION(400,"ROOM-1","ROOM을 찾을 수 없습니다."),
    ;

    private int status;
    private String errorCode;
    private String message;

}
