package com.web.exception;

public class QuestionRoomNotFoundExcetpion extends CommonException{
    public QuestionRoomNotFoundExcetpion(){
        super(ExceptionCode.QUESTION_ROOM_NOT_FOUND_EXCEPTION);
    }
}
