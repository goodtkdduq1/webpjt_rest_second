package com.web.service;

import com.web.dto.QuestionListDto;
import com.web.dto.QuestionDto;
import com.web.exception.QuestionRoomNotFoundExcetpion;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
public class QuestionService {
    private ArrayList<QuestionDto> questionList = new ArrayList<>();
    private HashMap<Integer, QuestionDto> questionListByRoomId = new HashMap<>();
    private HashMap<String,List<QuestionDto>> questionListByTags = new HashMap<>();

    public void addQuestion(QuestionDto questionDto){
        System.out.println(questionDto.getContext());
        questionList.add(questionDto);
        questionListByRoomId.put(questionDto.getRoomId(), questionDto);
        for (int i = 0; i< questionDto.getTagSize(); i++){
            String tag = questionDto.getTags().get(i);
            if (!questionListByTags.containsKey(tag)){
                questionListByTags.put(tag,new LinkedList<>());
            }
            questionListByTags.get(tag).add(questionDto);
        }
    }

    public void removeQuestionByRoomId(int roomId){
        validateQuestionRoom(roomId);
        QuestionDto temp = questionListByRoomId.get(roomId);
        questionList.remove(temp);
        for(int i =0;i<temp.getTagSize();i++){
            questionListByTags.get(temp.getTags().get(i)).remove(temp);
        }
        questionListByRoomId.remove(roomId);
    }

    public QuestionListDto getQuestionAllList()
    {
        QuestionListDto questionListDto = QuestionListDto.builder()
                .list(questionList)
                .build();
        return questionListDto;
    }

    public QuestionListDto getQuestionListByTags(List<String> tags){
        ArrayList<QuestionDto> ret = new ArrayList<>();
        for(int i =0;i<tags.size();i++){
            if(questionListByTags.containsKey(tags.get(i))) {
                List<QuestionDto> temp = questionListByTags.get(tags.get(i));
                for (int j = 0; j < temp.size(); j++) {
                    ret.add(temp.get(j));
                }
            }
        }
        QuestionListDto res = QuestionListDto.builder()
                .list(ret)
                .build();
        return res;
    }

    public QuestionDto getQuestionByRoomId(int roomId){
        System.out.println("getRoomId");
        return questionListByRoomId.get(roomId);
    }

    private void validateQuestionRoom(int roomId){
        if(!questionListByRoomId.containsKey(roomId)){
            throw new QuestionRoomNotFoundExcetpion();
        }
    }
}
