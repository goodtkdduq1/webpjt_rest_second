package com.web;

import com.web.dto.QuestionListDto;
import com.web.dto.QuestionDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationControllerTest {
    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    @DisplayName("Test Case 2 - @PostMapping(/v1/Question)")
    public void testCaes2(){
        String title = "Test Title";
        String context = "Test Context";
        int roomId = 1;
        String mkUser = "sangyub";
        List<String> tag = new ArrayList<>();
        tag.add("Test Tag1");
        tag.add("Test Tag2");
        QuestionDto questionDto = QuestionDto.builder()
                .title(title)
                .context(context)
                .roomId(roomId)
                .mkUser(mkUser)
                .tags(tag)
                .build();
        QuestionListDto result = testRestTemplate.postForObject("/v1/Question/", questionDto, QuestionListDto.class);
        assertEquals(result.getList().get(0).getTitle(), questionDto.getTitle());
        assertEquals(result.getList().get(0).getContext(), questionDto.getContext());
        assertEquals(result.getList().get(0).getRoomId(), questionDto.getRoomId());
        assertEquals(result.getList().get(0).getMkUser(), questionDto.getMkUser());
        HttpHeaders header = new HttpHeaders();
        HttpEntity<QuestionDto> entity = new HttpEntity(questionDto,header);
        //testRestTemplate.delete("/v1/Question",questionVO,QuestionListVO.class);
        ResponseEntity<QuestionListDto> res = testRestTemplate.exchange("/v1/Question", HttpMethod.DELETE,entity, QuestionListDto.class);
        assertEquals(res.getBody().getList().size(),0);
    }

    @Test
    @DisplayName("Test Case 3 - @GetMapping(/v1/questionList)")
    public void testCaes3(){

        QuestionListDto result = testRestTemplate.getForObject("/v1/Question/", QuestionListDto.class);
        //assertEquals(result.getList().size(),0);
    }

    @Test
    @DisplayName("Test Case 4 - @GetMapping(\"/v1/questionList\")")
    public void testCase4(){
        for(int i = 0 ;i<20;i++) {
            String title = "Test Title"+i;
            String context = "Test Context"+i;
            int roomId = i;
            String mkUser = "sangyub"+i;
            List<String> tag = new ArrayList<>();
            tag.add("Test Tag1_"+i);
            tag.add("Test Tag2_"+i);
            QuestionDto questionDto = QuestionDto.builder()
                    .title(title)
                    .context(context)
                    .roomId(roomId)
                    .mkUser(mkUser)
                    .tags(tag)
                    .build();
            QuestionListDto result = testRestTemplate.postForObject("/v1/Question/", questionDto, QuestionListDto.class);
            assertEquals(result.getList().get(i).getTitle(), questionDto.getTitle());
            assertEquals(result.getList().get(i).getContext(), questionDto.getContext());
            assertEquals(result.getList().get(i).getRoomId(), questionDto.getRoomId());
            assertEquals(result.getList().get(i).getMkUser(), questionDto.getMkUser());
        }
        QuestionListDto result = testRestTemplate.getForObject("/v1/questionList", QuestionListDto.class);
        for(int i =0;i<result.getList().size();i++)
        {
            assertEquals(result.getList().get(i).getTitle(), "Test Title"+i);
            assertEquals(result.getList().get(i).getContext(), "Test Context"+i);
            assertEquals(result.getList().get(i).getRoomId(), i);
            assertEquals(result.getList().get(i).getMkUser(), "sangyub"+i);
            for (int j =1;j<=result.getList().get(i).getTagSize();j++){
                assertEquals(result.getList().get(i).getTags().get(j-1), "Test Tag"+j+"_"+i);
            }
        }



        for(int i = 0 ;i<20;i++) {
            String title = "Test Title" + i;
            String context = "Test Context" + i;
            int roomId = i;
            String mkUser = "sangyub" + i;
            List<String> tag = new ArrayList<>();
            tag.add("Test Tag1_" + i);
            tag.add("Test Tag2_" + i);
            QuestionDto questionDto = QuestionDto.builder()
                    .title(title)
                    .context(context)
                    .roomId(roomId)
                    .mkUser(mkUser)
                    .tags(tag)
                    .build();
            HttpHeaders header = new HttpHeaders();
            HttpEntity<QuestionDto> entity = new HttpEntity(questionDto,header);
            //testRestTemplate.delete("/v1/Question",questionVO,QuestionListVO.class);
            ResponseEntity<QuestionListDto> res = testRestTemplate.exchange("/v1/Question", HttpMethod.DELETE,entity, QuestionListDto.class);

        }

        QuestionListDto result2 = testRestTemplate.getForObject("/v1/Question/", QuestionListDto.class);
        //assertEquals(result2.getList().size(),0);
    }

    /*
    @Test
    @DisplayName("Test Case 5 - \"/v1/questionListByTags\"")
    public void testCase5() {
        for(int i = 0 ;i<20;i++) {
            String title = "Test Title"+i;
            String context = "Test Context"+i;
            int roomId = i;
            String mkUser = "sangyub"+i;
            List<String> tag = new ArrayList<>();
            tag.add("Test Tag1");
            tag.add("Test Tag2_"+i);
            QuestionVO questionVO = QuestionVO.builder()
                    .title(title)
                    .context(context)
                    .roomId(roomId)
                    .mkUser(mkUser)
                    .tags(tag)
                    .build();
            QuestionListVO result = testRestTemplate.postForObject("/v1/Question/", questionVO, QuestionListVO.class);
            assertEquals(result.getList().get(i).getTitle(), questionVO.getTitle());
            assertEquals(result.getList().get(i).getContext(), questionVO.getContext());
            assertEquals(result.getList().get(i).getRoomId(), questionVO.getRoomId());
            assertEquals(result.getList().get(i).getMkUser(), questionVO.getMkUser());
        }

        Map< String, ArrayList<String> > params = new HashMap< String, ArrayList<String> >();
        ArrayList<String> tags = new ArrayList<>();
        tags.add("Test Tag1");
        params.put("tagList[]", tags);
        QuestionListVO result = testRestTemplate.getForObject("/v1/questionListByTags", QuestionListVO.class,params);
        System.out.println(result.getList().size());
        for (int i =0;i < result.getList().size();i++){
            assertEquals(result.getList().get(i).getTitle(), "Test Title"+i);
            assertEquals(result.getList().get(i).getContext(), "Test Context"+i);
            assertEquals(result.getList().get(i).getRoomId(), i);
            assertEquals(result.getList().get(i).getMkUser(), "sangyub"+i);

            assertEquals(result.getList().get(i).getTags().get(0), "Test Tag1");
            assertEquals(result.getList().get(i).getTags().get(1), "Test Tag2_"+i);
        }
        for (int i =0;i<20;i++) {
            Map< String, ArrayList<String> > params2 = new HashMap< String, ArrayList<String> >();
            ArrayList<String> tags2 = new ArrayList<>();
            tags2.add("Test Tag2_"+i);
            params2.put("tagList", tags2);
            QuestionListVO result1 = testRestTemplate.getForObject("/v1/questionListByTags?tagList=test Tag2_"+i+"", QuestionListVO.class);
            assertEquals(result1.getList().size(),1);
            assertEquals(result.getList().get(0).getTitle(), "Test Title"+i);
            assertEquals(result.getList().get(0).getContext(), "Test Context"+i);
            assertEquals(result.getList().get(0).getRoomId(), i);
            assertEquals(result.getList().get(0).getMkUser(), "sangyub"+i);
            assertEquals(result.getList().get(0).getTags().get(0), "Test Tag1");
            assertEquals(result.getList().get(0).getTags().get(1), "Test Tag2_"+i);
        }

    }
*/
    @Test
    @DisplayName("Test Case 6 - @GetMapping(\"/v1/questionList\")")
    public void testCase6(){
        for(int i = 0 ;i<20;i++) {
            String title = "Test Title"+i;
            String context = "Test Context"+i;
            int roomId = i;
            String mkUser = "sangyub"+i;
            List<String> tag = new ArrayList<>();
            tag.add("Test Tag1_"+i);
            tag.add("Test Tag2_"+i);
            QuestionDto questionDto = QuestionDto.builder()
                    .title(title)
                    .context(context)
                    .roomId(roomId)
                    .mkUser(mkUser)
                    .tags(tag)
                    .build();
            QuestionListDto result = testRestTemplate.postForObject("/v1/Question/", questionDto, QuestionListDto.class);
            assertEquals(result.getList().get(i).getTitle(), questionDto.getTitle());
            assertEquals(result.getList().get(i).getContext(), questionDto.getContext());
            assertEquals(result.getList().get(i).getRoomId(), questionDto.getRoomId());
            assertEquals(result.getList().get(i).getMkUser(), questionDto.getMkUser());
        }
        QuestionListDto resultAll = testRestTemplate.getForObject("/v1/questionList", QuestionListDto.class);
        assertEquals(resultAll.getList().size(), 20);
        for(int i =0;i<20;i++)
        {
            QuestionDto result = testRestTemplate.getForObject("/v1/questionByRoomId/"+i, QuestionDto.class);
            assertEquals(result.getTitle(), "Test Title"+i);
            assertEquals(result.getContext(), "Test Context"+i);
            assertEquals(result.getRoomId(), i);
            assertEquals(result.getMkUser(), "sangyub"+i);
            for (int j =1;j<=result.getTagSize();j++){
                assertEquals(result.getTags().get(j-1), "Test Tag"+j+"_"+i);
            }
        }



        for(int i = 0 ;i<20;i++) {
            String title = "Test Title" + i;
            String context = "Test Context" + i;
            int roomId = i;
            String mkUser = "sangyub" + i;
            List<String> tag = new ArrayList<>();
            tag.add("Test Tag1_" + i);
            tag.add("Test Tag2_" + i);
            QuestionDto questionDto = QuestionDto.builder()
                    .title(title)
                    .context(context)
                    .roomId(roomId)
                    .mkUser(mkUser)
                    .tags(tag)
                    .build();
            HttpHeaders header = new HttpHeaders();
            HttpEntity<QuestionDto> entity = new HttpEntity(questionDto,header);
            //testRestTemplate.delete("/v1/Question",questionVO,QuestionListVO.class);
            ResponseEntity<QuestionListDto> res = testRestTemplate.exchange("/v1/Question", HttpMethod.DELETE,entity, QuestionListDto.class);

        }

        QuestionListDto resultAll2 = testRestTemplate.getForObject("/v1/questionList", QuestionListDto.class);
        assertEquals(resultAll2.getList().size(), 0);
    }
}
