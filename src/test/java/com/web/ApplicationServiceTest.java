package com.web;

import com.web.service.QuestionService;
import com.web.dto.QuestionListDto;
import com.web.dto.QuestionDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import static junit.framework.TestCase.assertEquals;

@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationServiceTest {

    @Autowired
    private QuestionService questionService;

    @Test
    @DisplayName("Test Case 1 - Insert Question List and Get List All Item")
    public void Test_Case1() {
        String title = "Test Title";
        String context = "Test Context";
        int roomId = 1;
        String mkUser = "sangyub";
        List<String> tag = new ArrayList<>();
        tag.add("Test Tag1");
        tag.add("Test Tag2");
        QuestionDto questionDto = QuestionDto.builder()
                .title(title)
                .context(context)
                .roomId(roomId)
                .mkUser(mkUser)
                .tags(tag)
                .build();

        questionService.addQuestion(questionDto);

        QuestionListDto res = questionService.getQuestionAllList();

        assertEquals(questionDto.getTitle(), res.getList().get(0).getTitle());
        assertEquals(questionDto.getContext(), res.getList().get(0).getContext());
        assertEquals(questionDto.getRoomId(), res.getList().get(0).getRoomId());
        assertEquals(questionDto.getMkUser(), res.getList().get(0).getMkUser());
        questionService.removeQuestionByRoomId(1);
    }

    @Test
    @DisplayName("Test Case 2 - Insert Question List and Get List By Tags")
    public void Test_Case2() {
        String title = "Test Title";
        String context = "Test Context";
        int roomId = 1;
        String mkUser = "sangyub";
        List<String> tag = new ArrayList<>();
        tag.add("Test Tag1");
        tag.add("Test Tag2");
        QuestionDto questionDto = QuestionDto.builder()
                .title(title)
                .context(context)
                .roomId(roomId)
                .mkUser(mkUser)
                .tags(tag)
                .build();
        questionService.addQuestion(questionDto);
        // Tag로 질문 찾는 항목 Test
        List<String> searchTagCase1 = new ArrayList<>();
        searchTagCase1.add("Test Tag1");
        QuestionListDto res1 = questionService.getQuestionListByTags(searchTagCase1);
        assertEquals(questionDto.getTitle(), res1.getList().get(0).getTitle());
        assertEquals(questionDto.getContext(), res1.getList().get(0).getContext());
        assertEquals(questionDto.getRoomId(), res1.getList().get(0).getRoomId());
        assertEquals(questionDto.getMkUser(), res1.getList().get(0).getMkUser());


        List<String> searchTagCase2 = new ArrayList<>();
        searchTagCase2.add("Test Tag2");
        QuestionListDto res2 = questionService.getQuestionListByTags(searchTagCase2);
        assertEquals(questionDto.getTitle(), res2.getList().get(0).getTitle());
        assertEquals(questionDto.getContext(), res2.getList().get(0).getContext());
        assertEquals(questionDto.getRoomId(), res2.getList().get(0).getRoomId());
        assertEquals(questionDto.getMkUser(), res2.getList().get(0).getMkUser());


        List<String> searchTagCase3 = new ArrayList<>();
        searchTagCase3.add("Test Tag2");
        searchTagCase3.add("Test Tag1");
        QuestionListDto res3 = questionService.getQuestionListByTags(searchTagCase3);
        assertEquals(questionDto.getTitle(), res3.getList().get(0).getTitle());
        assertEquals(questionDto.getContext(), res3.getList().get(0).getContext());
        assertEquals(questionDto.getRoomId(), res3.getList().get(0).getRoomId());
        assertEquals(questionDto.getMkUser(), res3.getList().get(0).getMkUser());


        questionService.removeQuestionByRoomId(1);
    }
    @Test
    @DisplayName("Test Case 3 - Insert Question List and Get List No Search Item")
    public void Test_Case3(){
        String title = "Test Title";
        String context = "Test Context";
        int roomId = 1;
        String mkUser = "sangyub";
        List<String> tag = new ArrayList<>();
        tag.add("Test Tag1");
        tag.add("Test Tag2");
        QuestionDto questionDto = QuestionDto.builder()
                .title(title)
                .context(context)
                .roomId(roomId)
                .mkUser(mkUser)
                .tags(tag)
                .build();
        questionService.addQuestion(questionDto);
        List<String> searchTagCase2 = new ArrayList<>();
        searchTagCase2.add("Test Tag3");
        QuestionListDto res3 =questionService.getQuestionListByTags(searchTagCase2);
        assertEquals(0,res3.getList().size());
        questionService.removeQuestionByRoomId(1);

    }

    @Test
    @DisplayName("Test Case 4 - Insert Question List and Get List Remove Item")
    public void Test_Case4(){
        String title = "Test Title";
        String context = "Test Context";
        int roomId = 1;
        String mkUser = "sangyub";
        List<String> tag = new ArrayList<>();
        tag.add("Test Tag1");
        tag.add("Test Tag2");
        QuestionDto questionDto = QuestionDto.builder()
                .title(title)
                .context(context)
                .roomId(roomId)
                .mkUser(mkUser)
                .tags(tag)
                .build();
        questionService.addQuestion(questionDto);
        questionService.removeQuestionByRoomId(1);
        List<String> searchTagCase2 = new ArrayList<>();
        searchTagCase2.add("Test Tag2");
        QuestionListDto res3 =questionService.getQuestionListByTags(searchTagCase2);

        assertEquals(0,res3.getList().size());

    }

    @Test
    @DisplayName("Test Case 5 - Insert Question List and Get QuestionVO By RoomId")
    public void Test_Case5(){
        String title = "Test Title";
        String context = "Test Context";
        int roomId = 1;
        String mkUser = "sangyub";
        List<String> tag = new ArrayList<>();
        tag.add("Test Tag1");
        tag.add("Test Tag2");
        QuestionDto questionDto = QuestionDto.builder()
                .title(title)
                .context(context)
                .roomId(roomId)
                .mkUser(mkUser)
                .tags(tag)
                .build();
        questionService.addQuestion(questionDto);


        QuestionDto res1 =questionService.getQuestionByRoomId(1);
        assertEquals(questionDto,res1);

    }
}
